from flask import Flask, render_template
from main import generate

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template("index.html")

@app.route('/map')
def map():
    generate()
    return app.send_static_file('map.html')

if __name__ == "__main__":
    app.run()