# Import necessary libraries
import ephem
import folium
import requests

# Get TLE data from Celestrak
url = "https://celestrak.org/NORAD/elements/gp.php?GROUP=stations&FORMAT=tle"
response = requests.get(url).text.split("\n")

# Get Satellite Position
def get_satellite_position(tle_line1, tle_line2):
    satellite = ephem.readtle("Satellite", tle_line1, tle_line2)
    satellite.compute()
    sat_altitude = satellite.elevation
    return satellite.sublat/ephem.degree, satellite.sublong/ephem.degree, sat_altitude

# Create a folium map
f_map = folium.Map(location=[0, 0], zoom_start=4,max_bounds=True, max_zoom=5)

# Add a marker to the map
def add_marker(name: str, lat: float, lon: float, alt: float) -> None:
    marker_popup = f"""
        <h3>{name}</h3>
        <p>Latitude: {round(lat,3)}</p>
        <p>Longitude: {round(lon,3)}</p>
        <p>Altitude: {round(alt/1000, 2)}km</p>
        <a href="https://www.google.com/search?q={name}+Satellite" target="_blank" rel="noopener noreferrer">Web Search</a>
    """
    folium.Marker(location=[lat, lon], icon=folium.CustomIcon(icon_image='sat.png', icon_size=(40, 40)), popup=marker_popup, tooltip=name).add_to(f_map)

# Loop through the TLE data and add markers to the map
async def generate():
    for i in range(0, len(response)-1, 3):
        name = response[i].strip()
        lat, lon, alt = get_satellite_position(response[i+1], response[i+2])
        add_marker(name, lat, lon, alt)
    # Save the map as an HTML file
    await f_map.save('static/map.html')
